package com.clmconsultores.movienode.mapping;

import com.clmconsultores.movienode.entities.ReplaceMovie;
import com.clmconsultores.movienode.models.ReplaceMovieModel;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ReplaceMovieMap {

    public ReplaceMovieModel toModel(ReplaceMovie entity){
        if(entity != null){
            ReplaceMovieModel model = new ReplaceMovieModel();
            model.setMovie(entity.getMovie());
            model.setFind(entity.getFind());
            model.setReplace(entity.getReplace());
            return model;
        }
        else {
            return null;
        }
    }

    public ReplaceMovie toEntity(ReplaceMovieModel model){
        if(model != null){
            ReplaceMovie entity = new ReplaceMovie();
            entity.setMovie(model.getMovie());
            entity.setFind(model.getFind());
            entity.setReplace(model.getReplace());
            return entity;
        }
        else {
            return null;
        }
    }

    public List<ReplaceMovieModel> toModelList(List<ReplaceMovie> listEntity){
        List<ReplaceMovieModel> listModel = new ArrayList<>();
        for (ReplaceMovie entity : listEntity){
            listModel.add(toModel(entity));
        }
        return listModel;
    }

    public List<ReplaceMovie> toEntityList(List<ReplaceMovieModel> listModel){
        List<ReplaceMovie> listEntity = new ArrayList<>();
        for (ReplaceMovieModel model : listModel){
            listEntity.add(toEntity(model));
        }
        return listEntity;
    }
}
