package com.clmconsultores.movienode.mapping;

import com.clmconsultores.movienode.entities.Rating;
import com.clmconsultores.movienode.models.RatingModel;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class RatingMap {

    public RatingModel toModel(Rating entity){
        if(entity != null){
            RatingModel model = new RatingModel();
            model.setSource(entity.getSource());
            model.setValue(entity.getValue());
            return model;
        }
        else {
            return null;
        }
    }

    public Rating toEntity(RatingModel model){
        if(model != null){
            Rating entity = new Rating();
            entity.setSource(model.getSource());
            entity.setValue(model.getValue());
            return entity;
        }
        else {
            return null;
        }
    }

    public List<RatingModel> toModelList(List<Rating> listEntity){
        List<RatingModel> listModel = new ArrayList<>();
        for (Rating entity : listEntity){
            listModel.add(toModel(entity));
        }
        return listModel;
    }

    public List<Rating> toEntityList(List<RatingModel> listModel){
        List<Rating> listEntity = new ArrayList<>();
        for (RatingModel model : listModel){
            listEntity.add(toEntity(model));
        }
        return listEntity;
    }
}
