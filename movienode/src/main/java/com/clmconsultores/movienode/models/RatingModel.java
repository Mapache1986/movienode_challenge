package com.clmconsultores.movienode.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class RatingModel {

    @JsonProperty("Source")
    private String source;

    @JsonProperty("Value")
    private String value;
}
