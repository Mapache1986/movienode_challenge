package com.clmconsultores.movienode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovienodeApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovienodeApplication.class, args);
	}

}
