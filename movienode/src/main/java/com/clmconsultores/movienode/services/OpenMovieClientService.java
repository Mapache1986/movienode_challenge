package com.clmconsultores.movienode.services;

import com.clmconsultores.movienode.config.AppConfig;
import com.clmconsultores.movienode.entities.Movie;
import com.clmconsultores.movienode.mapping.MovieMap;
import com.clmconsultores.movienode.models.MovieModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.NotNull;

@Service
public class OpenMovieClientService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private AppConfig appConfig;

    @Autowired
    private MovieMap movieMap;

    public Movie findByImdbId(@NotNull String imdbId, @NotNull String apiKey){
        String endpoint = this.appConfig.getOmdbapiUrl() + "apikey=" + apiKey + "&i=" + imdbId;
        MovieModel model = this.restTemplate.getForObject(endpoint, MovieModel.class);
        return movieMap.toEntity(model);
    }

    public Movie findByTitle(@NotNull String title, @NotNull String apiKey){
        String endpoint = this.appConfig.getOmdbapiUrl() + "apikey=" + apiKey + "&t=" + title;
        MovieModel model = this.restTemplate.getForObject(endpoint, MovieModel.class);
        return movieMap.toEntity(model);
    }
}
