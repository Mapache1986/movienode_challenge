package com.clmconsultores.movienode.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Rating {

    @Field("source")
    private String source;

    @Field("value")
    private String value;
}
