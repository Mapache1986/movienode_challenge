package com.clmconsultores.movienode.repositories;

import com.clmconsultores.movienode.entities.Movie;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

@Repository
public interface IMovieRepository extends MongoRepository<Movie, ObjectId> {

    Page<Movie> findAll(Pageable pageable);

    Optional<Movie> findByTitle(String title);

    @Query("{ title: { $regex: /.*?0.*/, $options: \"i\" } }")
    List<Movie> findByPartialTitle(String title);

}
