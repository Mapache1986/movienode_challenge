# movienode_challenge

Desarrollo de prueba técnica para CLM Consultores. <br>
Esta prueba originalmente se solicitó realizar con NodeJS, pero debido a que no poseo conocimientos de dicho lenguaje, se me permitió desarrollar en un lenguaje que si conosca. <br>
El lenguaje escojido es Java 8 - Spring Boot 2.4.5 <br>
El reto tiene como fecha de expiración el día Viernes 14/05/2021 a las 23:59 horas. <br>

La descripción del reto es el siguiente: <br>

<h1>MovieNode Challenge!</h1>

Construir a través de NodeJS un microservicio con 2 endpoints que cumplan las siguientes especificaciones:

<h3>Buscador de películas:</h3>
1. Método GET <br>
2. El valor a buscar debe ir en la URL de la API <br>
3. Adicionalmente puede ir un header opcional que contenga el año de la película. <br>
4. Almacenar en una BD Mongo, la siguiente info: <br>

- Title
- Year
- Released
- Genre
- Director
- Actors
- Plot
- Ratings

5. El registro de la película solo debe estar una vez en la BD. <br>
6. Devolver la información almacenada en la BD. <br>

<h3>Obtener todas las películas:</h3>
1. Método GET <br>
2. Se deben devolver todas las películas que se han guardado en la BD. <br>
3. Si hay más de 5 películas guardadas en BD, se deben paginar los resultados de 5 en 5 <br>
4. El número de página debe ir por header. <br>

<h3>Buscar y reemplazar:</h3>
1. Método POST que reciba en el BODY un object como: P.E: {movie: star wars, find: jedi, replace: CLM Dev } <br>
2. Buscar dentro de la BD y obtener el campo PLOT del registro <br>
3. Al string del plot obtenido buscar la palabra enviada en el Body (find) y reemplazar todas sus ocurrencias por el campo enviado en el body (replace) <br>
4. Devolver el string con las modificaciones del punto anterior <br>

<h1>REGLAS:</h1>
1. Usar Koa como servidor. <br>
2. Validar los contratos de entrada y/o headers para los endpoints antes de realizar la lógica. <br>
3. Usar Mongoose como ORM. <br>
4. Subir el código respuesta del ejercicio a gitlab, y enviarnos la url con la solución del ejercicio. <br>
5. Para más info con respecto a la api de películas: http://www.omdbapi.com/ <br>
6. Para generar el apiKey:  http://www.omdbapi.com/apikey.aspx <br>

<h1>DESEABLES:</h1>
 
1. Uso de Joi para validación de contratos/headers <br>
2. Uso de Docker/Swarm para levantar todos los servicios. <br>
3. Uso de middleware para validación de contratos y manejo de errores. <br>
4. Uso de Logs para manejo de errores. <br>
5. Documentación del ejercicio en swagger. <br>

<h1>DOCUMENTACI&Oacute;N</h1>

El microservicio fue desarrollado con: <br>
Java 8 <br>
SDK 1.8 version 1.8.0_282 <br>
Maven <br>

<h3>BASE DE DATOS</h3>

Base de datos Mongo DB <br>
host: localhost <br>
port: 27017 <br>
database: movienodedb <br>

Si gustan pueden cambiar las credenciales de la base de datos en el siguiente archivo: <br>
Ruta: movienode/src/resources/application.properties

<h3>ENDPOINTS</h3>

Adem&aacute;s, se implementó Swagger para documentar los endpoints, por lo que puede utilizar la siguiente url, si levanta esta aplicaci&oacute;n en ambiente local: <br>
http://localhost:8080/swagger-ui.html

Todos los endpoints piden en su Header la apikey, la cual se validar&aacute; en cada request. <br>

1. /Movies/findByImdbId <br>
   HttpMethod: GET <br>
   HttpStatus: 201 CREATED: Si la pel&iacute;cula buscada hizo persistencia en la base de datos exit&oacute;samente. <br>
   HttpStatus: 409 CONFLICT: Si la pel&iacute;cula buscada no fue encontrada o ya existe en la base de datos local. <br>
   Par&aacute;metro: String con id Imdb de la pelicula. <br>

2. /Movies/findByName <br>
   HttpMethod: GET <br>
   HttpStatus: 201 CREATED: Si la pel&iacute;cula buscada hizo persistencia en la base de datos exit&oacute;samente. <br>
   HttpStatus: 409 CONFLICT: Si la pel&iacute;cula buscada no fue encontrada o ya existe en la base de datos local. <br>
   Par&aacute;metro: String con el nombre parcial de la pelicula deseada. <br>

3. /Movies/findAll <br>
   HttpMethod: GET <br>
   HttpStatus: 200 OK: Si la p&aacute;gina tiene elementos. <br>
   HttpStatus: 204 NO CONTENT: Si la p&aacute;gina no tiene elementos. <br>
   Par&aacute;metro: Integer con el n&uacute;mero de la p&aacute;gina deseada. <br>

4. /Movies/update <br>
   HttpMethod: POST <br>
   HttpStatus: 200 OK: si el plot de la pel&iacute;cula fue modificado exit&oacute;samente. <br>
   HttpStatus: 404 NOT FOUND: Si no se encontr&oacute; la pel&iacute;cula deseada. <br>
   Par&aacute;metro: JSON como Body del request, este objeto es de tipo ReplaceMovieModel <br>
