package com.clmconsultores.movienode.mapping;

import com.clmconsultores.movienode.entities.Movie;
import com.clmconsultores.movienode.models.MovieModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MovieMap {

    @Autowired
    private RatingMap ratingMap;

    public MovieModel toModel(Movie entity){
        if(entity != null){
            MovieModel model = new MovieModel();
            model.setTitle(entity.getTitle());
            model.setYear(entity.getYear());
            model.setReleased(entity.getReleased());
            model.setGenre(entity.getGenre());
            model.setDirector(entity.getDirector());
            model.setActors(entity.getActors());
            model.setPlot(entity.getPlot());
            model.setRatings(this.ratingMap.toModelList(entity.getRatings()));
            return model;
        }
        else {
            return null;
        }
    }

    public Movie toEntity(MovieModel model){
        if(model != null){
            Movie entity = new Movie();
            entity.setTitle(model.getTitle());
            entity.setYear(model.getYear());
            entity.setReleased(model.getReleased());
            entity.setGenre(model.getGenre());
            entity.setDirector(model.getDirector());
            entity.setActors(model.getActors());
            entity.setPlot(model.getPlot());
            entity.setRatings(this.ratingMap.toEntityList(model.getRatings()));
            return entity;
        }
        else {
            return null;
        }
    }

    public List<MovieModel> toModelList(List<Movie> listEntity){
        List<MovieModel> listModel = new ArrayList<>();
        for (Movie entity : listEntity){
            listModel.add(toModel(entity));
        }
        return listModel;
    }

    public List<Movie> toEntityList(List<MovieModel> listModel){
        List<Movie> listEntity = new ArrayList<>();
        for (MovieModel model : listModel){
            listEntity.add(toEntity(model));
        }
        return listEntity;
    }
}
