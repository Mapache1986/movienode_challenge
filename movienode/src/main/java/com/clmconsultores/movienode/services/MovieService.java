package com.clmconsultores.movienode.services;

import com.clmconsultores.movienode.config.AppConfig;
import com.clmconsultores.movienode.entities.Movie;
import com.clmconsultores.movienode.entities.ReplaceMovie;
import com.clmconsultores.movienode.repositories.IMovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Service
public class MovieService {

    @Autowired
    private IMovieRepository movieRepository;

    @Autowired
    private OpenMovieClientService openMovieClientService;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private AppConfig appConfig;

    public Movie save(@NotNull Movie movie){
        movie.setId(null);
        Optional<Movie> storedMovie = this.movieRepository.findByTitle(movie.getTitle());
        if(!storedMovie.isPresent()){
            return this.movieRepository.save(movie);
        }
        else {
            return null;
        }
    }

    public Movie apiFindByImdbId(@NotNull String imdbId, @NotNull String apiKey){
        Movie movie = this.openMovieClientService.findByImdbId(imdbId, apiKey);
        if(movie != null){
            return this.save(movie);
        }
        else {
            return null;
        }
    }

    public Movie apiFindByTitle(@NotNull String title, @NotNull String apiKey){
        Movie movie = this.openMovieClientService.findByTitle(title, apiKey);
        if(movie != null){
            return this.save(movie);
        }
        else {
            return null;
        }
    }

    public List<Movie> findAll(int page, int size){
        Pageable paging = PageRequest.of(page, size);
        return this.movieRepository.findAll(paging).getContent();
    }

    public Movie update(@NotNull ReplaceMovie order){
        Optional<Movie> movie = this.movieRepository.findByPartialTitle(order.getMovie()).stream().findFirst();

        if(movie.isPresent()){
            movie.get().setPlot(this.updatePlot(movie.get().getPlot(), order.getFind(), order.getReplace()));
            return this.movieRepository.save(movie.get());
        }
        else{
            return null;
        }
    }

    private String updatePlot(@NotNull String originalPlot, @NotNull String replaceText, @NotNull String newText){
        return originalPlot.replaceAll("(?i)" + replaceText, newText);
    }

}
