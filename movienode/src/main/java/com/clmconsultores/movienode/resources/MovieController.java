package com.clmconsultores.movienode.resources;

import com.clmconsultores.movienode.mapping.MovieMap;
import com.clmconsultores.movienode.mapping.ReplaceMovieMap;
import com.clmconsultores.movienode.models.MovieModel;
import com.clmconsultores.movienode.models.ReplaceMovieModel;
import com.clmconsultores.movienode.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/Movies")
@Validated
public class MovieController {

    @Autowired
    private MovieService movieService;

    @Autowired
    private MovieMap movieMap;

    @Autowired
    private ReplaceMovieMap replaceMovieMap;

    @RequestMapping(value = "/findByImdbId/{imdbId}", method = RequestMethod.GET)
    public ResponseEntity<MovieModel> findByImdbId(@RequestHeader(value="apikey") String apikey, @NotNull @PathVariable String imdbId){
        MovieModel model = movieMap.toModel(this.movieService.apiFindByImdbId(imdbId, apikey));
        if(model != null){
            return ResponseEntity.status(HttpStatus.CREATED).body(model);
        }
        else {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
        }
    }

    @RequestMapping(value = "/findByName/{title}", method = RequestMethod.GET)
    public ResponseEntity<MovieModel> findByName(@RequestHeader(value="apikey") String apikey, @NotNull @PathVariable String title){
        MovieModel model = movieMap.toModel(this.movieService.apiFindByTitle(title, apikey));
        if(model != null){
            return ResponseEntity.status(HttpStatus.CREATED).body(model);
        }
        else {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
        }
    }

    @RequestMapping(value = "/findAll/{page}", method = RequestMethod.GET)
    public ResponseEntity<List<MovieModel>> findAll(
            @RequestHeader(value="apikey") String apikey,
            @NotNull @PathVariable int page,
            @RequestParam(defaultValue = "5") int size)
    {
        List<MovieModel> listModel = this.movieMap.toModelList(this.movieService.findAll(page, size));
        HttpHeaders header = new HttpHeaders();
        header.set("page", String.valueOf(page));
        if(!listModel.isEmpty()){
            return new ResponseEntity<>(listModel, header, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(null, header, HttpStatus.NO_CONTENT);
        }
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseEntity<String> update(@RequestHeader(value="apikey") String apikey, @NotNull @RequestBody ReplaceMovieModel replaceMovieModel){
        MovieModel model = this.movieMap.toModel(this.movieService.update(this.replaceMovieMap.toEntity(replaceMovieModel)));
        if(model != null){
            return ResponseEntity.status(HttpStatus.OK).body(model.getPlot());
        }
        else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

}
