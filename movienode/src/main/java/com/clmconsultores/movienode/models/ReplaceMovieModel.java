package com.clmconsultores.movienode.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ReplaceMovieModel {

    @JsonProperty("Movie")
    private String movie;

    @JsonProperty("Find")
    private String find;

    @JsonProperty("Replace")
    private String replace;
}
