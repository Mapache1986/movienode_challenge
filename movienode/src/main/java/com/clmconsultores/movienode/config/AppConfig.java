package com.clmconsultores.movienode.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties
@PropertySource("application.properties")
public class AppConfig {

    @Value("${omdbapi.url}")
    private String omdbapiUrl;

    @Value("${omdbapi.apikey}")
    private String omdbapiApiKey;


    public String getOmdbapiUrl() {
        return omdbapiUrl;
    }

    public String getOmdbapiApiKey() {
        return omdbapiApiKey;
    }
}
