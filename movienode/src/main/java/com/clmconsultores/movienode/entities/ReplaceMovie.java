package com.clmconsultores.movienode.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ReplaceMovie {

    @Field("movie")
    private String movie;

    @Field("find")
    private String find;

    @Field("replace")
    private String replace;
}
