package com.clmconsultores.movienode.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

// TODO: add field validations

@Document("movies")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Movie {

    @Id
    private String id;

    @Field("title")
    private String title;

    @Field("year")
    private String year;

    @Field("released")
    private String released;

    @Field("genre")
    private String genre;

    @Field("director")
    private String director;

    @Field("actors")
    private String actors;

    @Field("plot")
    private String plot;

    @Field("ratings")
    private List<Rating> ratings;


}
